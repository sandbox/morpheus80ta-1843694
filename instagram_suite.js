/**
 * @file
 * Javascript for Field Example.
 */

/**
 * Provides a farbtastic colorpicker for the fancier widget.
 */
(function ($) {
  Drupal.behaviors.InstagramSuiteInfiniteScroll = {
    attach: function(context) {
    	$('a.instagram-suite-infinite-scroll').once(function(){
    		var link = $(this);
    		/*
    		$(document).scroll(function(){
    			console.log($(document).scrollTop());
    			console.log(link.offset());
    			
    	        if  ($(window).scrollTop() >= link.offset().top){
        			loadNextPage(link);
    	        }
    		});
    		*/
    		$(this).click(function(){
    			link.addClass('load');
    			loadNextPage(link);
    			return false;
    		});
    	});
    }
  }
  loadNextPage = function(link){
	$.getJSON(link.attr('href'),function(response){
		if(response.success){
			link.attr('href',response.next_url);
			link.prevAll('.instagram-suite-item-container').append(response.data);
			link.removeClass('load');
			}
		}
	);
  }
})(jQuery);