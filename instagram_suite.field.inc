<?php

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function instagram_suite_field_info() {
  return array(
    'instagram_image' => array(
      'label' => t('Instragram'),
      'description' => t('This field insert an instagram gallery'),
      'default_widget' => 'instagram_suite_default_widget',
      'default_formatter' => 'instagram_suite_simple_text',
    ),
  );
}

/**
 * Implements hook_field_validate().
 *
 * @see instagram_suite_field_widget_error()
 */
function instagram_suite_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  watchdog('items validate', print_r($items, TRUE));     
  foreach ($items as $delta => $item) {
    /** @TODO check the input  
    
    if (!empty($item['rgb'])) {
      if (! preg_match('@^#[0-9a-f]{6}$@', $item['rgb'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'instagram_suite_invalid',
          'message' => t('Color must be in the HTML format #abcdef.'),
        );
      }
    }
    */
  }
}




/**
 * Implements hook_field_is_empty().
 *
 * This function check if we have a selected steam type and a not empty streamtag.
 */
function instagram_suite_field_is_empty($item, $field) {
  return FALSE;
  if(empty($item['streamtype'])){
    return TRUE;
  } else {
    return ( empty($item['streamtag']) )? TRUE : FALSE;
  }
  
}


/**
 * Implements hook_field_formatter_info().
 *
 * @see instagram_suite_field_formatter_view()
 */
function instagram_suite_field_formatter_info() {
  return array(
    // This formatter displays the images gallery.
    'instagram_gallery' => array( 
      'label' => t('Instagram Gallery'),
      'field types' => array('instagram_image'), //field type per cui è disponibile il formatter
      'settings'  => array( //Array dei setting del formatter
        'thumb_style' => 'default', //stile della thumbnails
        'image_style' => 'default', //stile dell'immagine
        'style' => 'instagram_classic',
        'infinite_scroll' => true
      )
    )
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 * 
 */
function instagram_suite_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $element = array();
  if($field['type'] == 'instagram_image' && isset($instance['display'][$view_mode]) && $instance['display'][$view_mode]['type'] == 'instagram_gallery'){
    
    //This gets the actual settings
    $settings = $instance['display'][$view_mode]['settings'];

    //Initialize the element variable
    $element = array();
    $entity_info = entity_get_info('file');
    
    // Retrive the media presets from image module
    $image_styles = image_styles();
    foreach ($image_styles as $key => $image_style) {
      $options[$key] = $image_style['name'];
    }
  
    //Add your select box
  
    $element['thumb_style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Thumbnails size'),                   // Widget label
      '#description'    => t('Select what size of thumbnails image'), // Helper text
      '#default_value'  => $settings['thumb_style'],              // Get the value if it's already been set
      '#options'        => $options,
    );
    $element['image_style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Image size'),                   // Widget label
      '#description'    => t('Select what size of image'), // Helper text
      '#default_value'  => $settings['image_style'],              // Get the value if it's already been set
      '#options'        => $options,
    );
    $element['style'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Style'),                   // Widget label
      '#description'    => t('Select a style'), // Helper text
      '#default_value'  => $settings['css_style'],              // Get the value if it's already been set
      '#options'        => array('instagram_classic'=>'Instagram Classic', 'instagram_black'=>'Black theme'),
    );
    $element['infinite_scroll'] = array(
      '#type'           => 'select',                           // Use a select box widget
      '#title'          => t('Infinite scroll'),                   // Widget label
      '#description'    => t('Enable or disable infinite scroll'), // Helper text
      '#default_value'  => $settings['infinite_scroll'],              // Get the value if it's already been set
      '#options'        => array(1 => t('Yes'), 0 => t('No')),
    );
  }
  
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 * 
 * composizione del sommario dei setting
 */
function instagram_suite_field_formatter_settings_summary($field, $instance, $view_mode) {
  $summary = '';
  if($field['type'] == 'instagram_image' && isset($instance['display'][$view_mode]) && $instance['display'][$view_mode]['type'] == 'instagram_gallery'){
    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];
    $summary = t('Use a \'@thumb\' Thumbs style, \'@image\' image style with thumbnail list', array(
      '@thumb'     => $settings['thumb_style'],
      '@image'  => $settings['image_style']
    )); 
  }
  return $summary;
}



/**
 * Implements hook_field_formatter_view().
 *
 * Two formatters are implemented.
 * - instagram_suite_simple_text just outputs markup indicating the color that
 *   was entered and uses an inline style to set the text color to that value.
 * - instagram_suite_color_background does the same but also changes the
 *   background color of div.region-content.
 *
 * @see instagram_suite_field_formatter_info()
 */
function instagram_suite_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    // This formatter simply outputs the field as text and with a color.
    case 'instagram_gallery';
      
      $settings = $display['settings'];
      
      if( $settings['style'] != 'custom'){
        drupal_add_css(drupal_get_path('module','instagram_suite') . '/css/'. $settings['style'] .'.css');
        $template_file = drupal_get_path('module', 'instagram_suite') . '/templates/' . $settings['style'] . '.tpl.php';
      } else {
        $template_file = 'pippo';
      }
      
      foreach ($items as $delta => $item) {
        $item = unserialize($item['value']);
        
        $element[$delta] = array(
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => array('class' => 'instagram-suite-gallery ' . str_replace("_","-",$settings['style']))
        );
        $method = $instance['widget']['type'];
        $kwargs = $item;
        
        $gallery = instagram_suite_make_request($method,$kwargs);

        $settings['field_name'] = $field['field_name'];
        $element[$delta]['#value'] = '<div class="instagram-suite-item-container">';
        $element[$delta]['#value'] .= theme('instagram_suite_item',array('gallery' => $gallery, 'settings' => $settings));
        $element[$delta]['#value'] .= '</div><div class="clear"></div>';
        if($settings['infinite_scroll'] && !empty($gallery['next_url'])){
          drupal_add_js(drupal_get_path('module','instagram_suite') . '/instagram_suite.js');
          $element[$delta]['#value'] .= l('<span>' . t('Load next') . '</span>','instagram-suite/field/next/'.$gallery['cid'].'/'.$entity_type.'/'.$instance['bundle'].'/'.$instance['field_name'],array('html' => true, 'absolute' => true, 'attributes' => array('class' => 'instagram-suite-infinite-scroll')));
        }

      }
    break;
      
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 *
 * @see instagram_suite_field_widget_form()
 */
function instagram_suite_field_widget_info() {
  $widgets = array();
  $widgets['instagram_suite_default_widget'] = array(
      'label' => t('Default widget'),
      'field types' => array('instagram_image'),
    );
  $widgets += _instagram_suite_get_endpoint_widget();
  return $widgets;
}


/**
 * Implements hook_field_widget_form().
 */
function instagram_suite_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  switch ($instance['widget']['type']) {
    
    case 'instagram_suite_default_widget':
      $element += array(
        '#type' => 'fieldset',
      );
    
      $element['streamtype'] = array(
        '#type' => 'radios', 
        '#title' => t('Stream type.'),  
        '#description' => t('Search images from Instagram by:'), 
        '#options' => array('author' => t('Author name'), 'tag' => t('Tag')), 
        '#required' => $instance['required'],  
        '#weight' => isset($element['#weight']) ? $element['#weight'] : 0, 
        '#default_value' => isset($items[$delta]['streamtype']) ? $items[$delta]['streamtype'] : NULL
      );
      
      $element['streamtag'] = array(
        '#type' => 'textfield',  
        '#title' => t('Stream tag'),  
        '#description' => t('Insert author name or tag depending to the selected steam type'),  
        '#required' => $instance['required'],  
        '#weight' => isset($element['#weight']) ? ($element['#weight'] + 1) : 0, 
        '#default_value' => isset($items[$delta]['streamtag']) ? $items[$delta]['streamtag'] : NULL
      );
    
      break;
      default:
        $value = drupal_array_get_nested_value($items, array($delta,'value'));
        if($value){
          $value = unserialize($value);
        }
        $endpoints = _instagram_suite_get_endpoint_parameters();
        if(isset($endpoints[$instance['widget']['type']])){
          $element += array(
            '#type' => 'fieldset',
            '#element_validate' => array('instagram_suite_field_widget_validate')
          );
          foreach($endpoints[$instance['widget']['type']] as $key => $form_field){
            $element[$key] = array(
              '#type' => $form_field['type'],
              '#title' => $form_field['label'],
              '#default_value' => ($value && isset($value[$key])) ? $value[$key] : '',
            );
          }
        }
        break;
  }
  return $element;
}


/**
 * Implements hook_field_widget_error().
 *
 * @see instagram_suite_field_validate()
 * @see form_error()
 */
function instagram_suite_field_widget_error($element, $error, $form, &$form_state) {
  /*
  switch ($error['error']) {
    case 'instagram_suite_invalid':
      form_error($element, $error['message']);
      break;
  }
  */
}

function instagram_suite_field_widget_validate($element, &$form_state) {
  $input = drupal_array_get_nested_value($form_state, array('values',$element['#field_name'],$element['#language'],$element['#delta']));
  form_set_value($element,array('value' => serialize($input)),$form_state);
}
