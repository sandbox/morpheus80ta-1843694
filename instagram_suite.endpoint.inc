<?php

function _instagram_suite_get_endpoint_parameters(){
  $countParam = array(
      'label' => t('Count of media to return'),
      'type'  => 'textfield'
    );
  $max_idParam = array(
      'label' => t('Return media after id'),
      'type'  => 'instagram_media_id'
    );
  $min_idParam = array(
      'label' => t('Return media before id'),
      'type'  => 'instagram_media_id'
    );
  $user_idParam = array(
        'label' => t('Instagram username'),
        'type' => 'instagram_user_id'
      );
  $min_timestampParam = array(
    'label' => t('From date'),
    'type' => 'instagram_timestamp'
  );
  $max_timestampParam = array(
    'label' => t('Until date'),
    'type' => 'instagram_timestamp'
  );

  $media_accept_parameters = array(
    'count' => $countParam,
//    'max_id' => $max_idParam,
  );
  $search_accept_parameters = array(
    'q' => array(
      'label' => t('%type to search'),
      'type'  => 'textfield'
    ),
    'count' => $countParam
  );
  $endpoints['user_recent_media'] = $media_accept_parameters + array(
//      'min_id' => $min_idParam,
      'user_id' => $user_idParam,
      'min_timestamp' => $min_timestampParam,
      'max_timestamp' => $max_timestampParam,
  );
  $endpoints['user_recent_media']['user_id']['required'] = TRUE;
//  $endpoints['tag_recent_media'] = $media_accept_parameters + 
  $endpoints['tag_recent_media'] =  array(
    'tag_name' => array(
      'label' => t('Tag to search'),
      'type'  => 'instagram_tag'
    ),
  );
  $endpoints['tag_recent_media']['tag_name']['required'] = TRUE;

  return $endpoints;
}

function _instagram_suite_get_endpoint_widget(){
  return array(
    'user_recent_media' => array(
      'label' => t('User recent media'),
      'field types' => array('instagram_image'),
    ),
    'tag_recent_media' => array(
      'label' => t('Tag recent media'),
      'field types' => array('instagram_image'),
    ),
    /*
    'location_recent_media' => array(
      'label' => t('Tag recent media'),
      'field types' => array('instagram_image'),
    ),
     * 
     */
  );
}

function instagram_suite_api_auth_method($method){
  $methods = array(
    'user_search' => 'access_token',
    'user_recent_media' => 'access_token',
  );
  return isset($methods[$method]) ? $methods[$method] : 'client_id';
}
